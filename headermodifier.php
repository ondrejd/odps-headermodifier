<?php
/**
 * Module "headermodifier" for Prestashop 1.6.0.9
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @link http://doc.prestashop.com/display/PS16/Creating+a+first+module
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Main module class.
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 */
class HeaderModifier extends Module {

  const PREF_SCRIPT = 'PS_HEADERMODIFIER_SCRIPT';

  /**
   * @return void
   */
  public function __construct() {
    $this->name = 'headermodifier';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Ondřej Doněk';
    $this->need_instance = 0;
    $this->bootstrap = true;
    $this->_directory = dirname(__FILE__);

    parent::__construct();

    $this->displayName = $this->l('Header Modifier');
    $this->description = $this->l('Module for enabling adding JavaScript to PrestaShop header.');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
  }

  /**
   * @return boolean
   */
  public function install() {
    if (!parent::install()) {
      return false;
    }

    Configuration::updateValue(PREF_SCRIPT, '', true);

    $this->registerHook('displayHeader');
    //return parent::install() && $this->registerHook('displayHeader');

    return true;
  }

  /**
   * @return boolean
   */
  public function uninstall() {
    if (!parent::uninstall()) {
      return false;
    }

    Configuration::deleteByName(self::PREF_SCRIPT);

    return true;
  }

  /**
   * @return void
   */
  public function getContent() {
    if (Tools::isSubmit('submitHeaderModifier')) {
      $pref_script = Tools::getValue(self::PREF_SCRIPT);
      Configuration::updateValue(self::PREF_SCRIPT, $pref_script, true);

      $this->html .= $this->displayConfirmation($this->l('Settings updated'));

      Tools::redirectAdmin(
          $this->context->link->getAdminLink('AdminModules', true).
          '&conf=6'.$this->getContentUrlSuffix()
      );
    }

    return $this->getContentForm();
  }

  /**
   * @return void
   */
  public function getContentForm() {
    $helper = new HelperForm();
    $helper->submit_action = 'submitHeaderModifier';
    $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . $this->getContentUrlSuffix();
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->tpl_vars = array('fields_value' => $this->getContentFormValues());

    $fields = array();
    $fields[] = array(
        'type' => 'textarea',
        'label' => $this->l('Script for the header'),
        'name' => self::PREF_SCRIPT,
        'desc' => $this->l('JavaScript to be appended to the header'),
    );

    return $this->html.$helper->generateForm(array(
      array(
        'form' => array(
          'legend' => array(
            'title' => $this->l('Settings'),
            'icon' => 'headermodifier'
          ),
          'input' => $fields,
          'submit' => array(
            'title' => $this->l('Save')
          )
        )
      )
    ));
  }

  /**
   * @return array
   */
  public function getContentFormValues() {
    $values = array();
    $values[self::PREF_SCRIPT] = Tools::getValue(self::PREF_SCRIPT, Configuration::get(self::PREF_SCRIPT));

    return $values;
  }

  /**
   * @return string
   */
  public function getContentUrlSuffix()
  {
    return '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
  }

  /**
   * @param array $params
   * @return string
   */
  public function hookDisplayHeader($params) {
    global $cookie, $smarty;

    if (!isset($this->context->controller->php_self)) {
      return;
    }

    $script = Tools::getValue(self::PREF_SCRIPT, Configuration::get(self::PREF_SCRIPT));

    $smarty->assign('headermodifier_script', $script);

    return $this->display(
        __FILE__,
        'header.tpl'/*,
        $this->getCacheId('headermodifier|header')*/
    );
  }
}
